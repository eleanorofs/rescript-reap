open IndexedDB;

type t = {
  dbResult: IDBRequest.t<IDBDatabase.t>
};

@val external window: Dom.window = "window";

let createStores = (stores: array<StoreConfig.t>,
                    transaction:IDBTransaction.t): unit
  => {
    let _ = stores -> Js.Array2.map(store => {
      store -> StoreConfig.createOrUpgrade(transaction);
    });
  };                      

let create = (config: DatabaseConfig.t<'onCreated>): unit => {
  let openRequest: IDBRequest.t<IDBDatabase.t> = window -> Window.indexedDB
      -> IDBFactory.Open.withVersion(config.databaseName, config.version
                                     -> Belt.Int.toFloat);
  openRequest -> IDBOpenDBRequest.set_onupgradeneeded(event => {
    let _ = event -> IDBVersionChangeEvent.target
        -> IDBRequest.transaction
        -> Js.Nullable.toOption
        -> Belt.Option.map(trans => createStores(config.stores, trans));
    config.onUpgraded(openRequest -> IDBRequest.result);
  });

  openRequest -> IDBOpenDBRequest.set_onsuccess(event => {
    let _ = event -> RequestSuccessEvent.target
        -> IDBRequest.result
        -> config.onCreated;
  });

  openRequest -> IDBOpenDBRequest.set_onerror(config.onCreationError);
};
                        
  
