type t = {
  storeName: string,
  indexName: string,
  parameters: IndexedDB.IDBIndexParameters.t,
  path: array<string>,
  version: int
};

let create = (t: t, objectStore: IndexedDB.IDBObjectStore.t):
    IndexedDB.IDBIndex.t =>
    objectStore
    -> IndexedDB.IDBObjectStore.CreateIndex
    .withParameters(t.indexName, #Array(t.path), t.parameters);
/*
let createIfNew = (t: t, os: IndexedDB.IDBObjectStore.t):
    IndexedDB.IDBIndex.t =>
  */  
