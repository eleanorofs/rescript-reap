type t = { unique: bool, locale: option<string> };

let toIDBIndexParameters = (t: t): IndexedDB.IDBIndexParameters.t => {
  unique: t.unique,
  locale: t.locale,
  multiEntry: false /* the lambda-to-string-path-array algo necesitates this.*/
};
