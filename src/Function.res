type t<'entry, 'path> = 'entry => 'path;

type opaque;

@send external
toString: opaque => string = "toString";

let tokenize = (opaque: opaque): array<string> => 
    opaque -> toString
    -> Js.String2.splitByRe(%re("/[\s+;]/"))
    -> Js.Array2.reduce((result, token) => switch (token) {
        | None => result;
        | Some(t) => result -> Js.Array2.concat([t])
    }, []);

let getExpression = (opaque: opaque): string => {
  let expr = opaque -> tokenize
      -> Js.Array2.find(token => token -> Js.String2.includes("."))
      -> Belt.Option.getWithDefault("");
  expr;  
};

let getKeyPath = (expr: string): array<string> =>
    expr -> Js.String2.split(".") -> Js.Array2.sliceFrom(1);

external toOpaque: t<'entry, 'path> => opaque = "%identity";

@send external toPath: ('entry => 'key) => Path.t<'entry> = "%identity";
