open IndexedDB;

module Private = {
  let toFuture = (txn: IDBTransaction.t, request: IDBRequest.t<'a>) => {
    Future.make(resolve => {
      txn -> IDBTransaction.set_oncomplete(_ => {
        resolve(Belt.Result.Ok(request -> IDBRequest.result))
      });
      txn -> IDBTransaction.set_onerror(_ => {
        let _ = txn -> IDBTransaction.error
            -> Js.Nullable.toOption
            -> Belt.Option.map(err => resolve(Belt.Result.Error(err)));
      });
    });
  };
};

type t<'entry> = {
  indices: array<Index.t<'entry>>,
  key: 'entry => string, //way to derive a key
  name: string,
  version: int,
};

let toConfig = (t: t<'entry>): StoreConfig.t => {
  name: t.name,
  indices: t.indices
    -> Js.Array2.map(index => index -> Index.toConfig(t.name)),
  key: t.key -> Function.toPath -> Path.toKeyArray,
  version: t.version
};
 
let add = (t: t<'entry>, db: IDBDatabase.t, value: 'entry):
    Future.t<Belt.Result.t<unit, Js.Exn.t>>
    => {
      let txn = db -> IDBDatabase.Transaction.withMode([ t.name ], #readwrite);
      let request = txn
          -> IDBTransaction.objectStore(t.name)
          -> IDBObjectStore.add(value);
      Private.toFuture(txn, request);
    };

let clear  = (t: t<'entry>, db: IDBDatabase.t):
    Future.t<Belt.Result.t<unit, Js.Exn.t>>
    => {
      let txn = db -> IDBDatabase.Transaction.withMode([ t.name ], #readwrite);
      let request = txn
          -> IDBTransaction.objectStore(t.name)
          -> IDBObjectStore.clear;
      Private.toFuture(txn, request);
    };

let count = (t: t<'entry>, db: IDBDatabase.t):
    Future.t<Belt.Result.t<float, Js.Exn.t>>
    => {
      let txn = db -> IDBDatabase.Transaction.withMode([ t.name], #readonly );
      let request = txn -> IDBTransaction.objectStore(t.name)
          -> IDBObjectStore.Count.withoutQuery;
      Private.toFuture(txn, request);
    };

let delete = (t: t<'entry>, db: IDBDatabase.t, key: string):
    Future.t<Belt.Result.t<unit, Js.Exn.t>>
    => {
      let txn = db -> IDBDatabase.Transaction.withMode([ t.name ], #readwrite);
      let request = txn -> IDBTransaction.objectStore(t.name)
          -> IDBObjectStore.delete(IDBKeyRange.only(key));
      Private.toFuture(txn, request);
    };
      
let get = (t: t<'entry>, db: IDBDatabase.t, key: string):
    Future.t<Belt.Result.t<'entry, Js.Exn.t>>
    => {
      let txn = db -> IDBDatabase.Transaction.withMode([ t.name ], #readonly);
      let request = txn -> IDBTransaction.objectStore(t.name)
          -> IDBObjectStore.get(IDBKeyRange.only(key));
      Private.toFuture(txn, request);
    };

let getAll = (t: t<'entry>, db: IDBDatabase.t):
    Future.t<Belt.Result.t<array<'entry>, Js.Exn.t>>
    => {
      let txn = db -> IDBDatabase.Transaction.withMode([ t.name ], #readonly);
      let request = txn -> IDBTransaction.objectStore(t.name)
          -> IDBObjectStore.getAll;
      Private.toFuture(txn, request);
    };

let put = (t: t<'entry>, db: IDBDatabase.t, value: 'entry):
    Future.t<Belt.Result.t<unit, Js.Exn.t>>
    => {
      let txn = db -> IDBDatabase.Transaction.withMode([ t.name ], #readwrite);
      let request = txn -> IDBTransaction.objectStore(t.name)
          -> IDBObjectStore.put(value);
      Private.toFuture(txn, request);
    };
