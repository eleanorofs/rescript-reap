type t<'entry>;

@send external toPath: ('entry => 'key) => t<'entry> = "%identity";

/*external toOpaque: t<'entry> => Function.opaque = "%identity";*/

/*****************************************************************************/

@send external toString: t<'entry> => string = "toString";

let tokenize = (t: t<'entry>): array<string> => 
    t -> toString
    -> Js.String2.splitByRe(%re("/[\s+;]/"))
    -> Js.Array2.reduce((result, token) => switch (token) {
        | None => result;
        | Some(t) => result -> Js.Array2.concat([t])
    }, []);

let getExpression = (t: t<'entry>): string => {
  let expr = t -> tokenize
      -> Js.Array2.find(token => token -> Js.String2.includes("."))
      -> Belt.Option.getWithDefault("");
  expr;  
};

let getKeyPath = (expr: string): array<string> =>
    expr -> Js.String2.split(".") -> Js.Array2.sliceFrom(1);


let toKeyArray = (t: t<'entry>): array<string> => {
  t -> getExpression -> getKeyPath
}
