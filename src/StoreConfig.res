/* an untyped configuration which is only used internally. */

open IndexedDB;

type t = {
  name: string,
  indices: array<IndexConfig.t>,
  key: array<string>,
  version: int
};

let create = (t: t, db: IDBDatabase.t):
    array<IDBIndex.t> => {
      let os = db
          -> IDBDatabase.CreateObjectStore.withOptions(t.name, {
            keyPath: Array(t.key),
            autoIncrement: false
          });
      t.indices
        -> Js.Array2.map(indexConf => IndexConfig.create(indexConf, os));
    };

let createNewIndices = (t: t, txn: IDBTransaction.t): array<IDBIndex.t> => {
  t.indices
    -> Js.Array2.filter(index => { 
      index.version -> Belt.Int.toFloat >= txn -> IDBTransaction.db -> IDBDatabase.version
    })
    -> Js.Array2.map(index => {
      index -> IndexConfig.create(txn -> IDBTransaction.objectStore(t.name))
    });
};

let createOrUpgrade = (t: t, transaction: IDBTransaction.t):
    array<IDBIndex.t> => {
      t.version -> Belt.Int.toFloat < transaction -> IDBTransaction.db -> IDBDatabase.version
        ? t -> createNewIndices(transaction)
        : t -> create(transaction -> IDBTransaction.db)
    };
