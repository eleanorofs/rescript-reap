type t<'onCreated> = {
  databaseName: string,
  version: int,
  stores: array<StoreConfig.t>,
  onCreated: IndexedDB.IDBDatabase.t => 'onCreated,
  onCreationError: Dom.errorEvent => unit, 
  onUpgraded: IndexedDB.IDBDatabase.t => unit,
};


