type t<'entry> = {
  name: string,
  parameters: IndexParameters.t,
  path: Path.t<'entry>,
  version: int
};

let toConfig = (t: t<'entry>, storeName): IndexConfig.t => {
  storeName: storeName,
  indexName: t.name,
  parameters: t.parameters -> IndexParameters.toIDBIndexParameters,
  path: t.path -> Path.toKeyArray,
  version: t.version
};


