let filterUnmatched = (actual: array<'t>, expected: array<'t>,
                       itemEquality: ('t, 't) => bool)
  => Js.Array2.filteri(expected, (e, i) =>
    itemEquality(e, actual -> Js.Array2.unsafe_get(i)));

let arrayEqual = (actual: array<'t>, expected: array<'t>,
                  itemEquality: ('t, 't) => bool)
  => {
    Js.Array2.length(actual) === Js.Array2.length(expected)
      && (Js.Array2.length(actual)
          === filterUnmatched(actual, expected, itemEquality)
          -> Js.Array2.length)
  };

let keyPathEqual = (actual: array<string>, expected: array<string>) => 
    arrayEqual(actual, expected, (a, e) => a === e);

type person = { name: string };

let path: Function.t<person, string> = (p) => p.name;

let suite: Vigil.Suite.t = {
  suiteName: "function tests",
  asserts: [
    Vigil.Assert.equality({
      comparator: keyPathEqual,
      expected: [ "name" ],
      message: "The path of person.name should be '[name]'.",
      operator: "Path.toKeyArray",
      fn: () => Path.toKeyArray(Function.toPath(p => p.name))
    })
  ]
};
