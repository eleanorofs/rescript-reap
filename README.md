This is currently in a pre-alpha state. 

## Goals: 

* a functional abstraction over IndexedDB over ReScript. 
* simple, succinct, readable database operations. 
* static typing guarantees for database records and their properties
* monadic return values to eliminate null/undefined checks
* typo-proofing property names in keys and indices. 
* ability to easily "escape" the library and work directly with IndexedDB 
bindings when necessary. 

## Limitations: 

The following are intentional simplifications which may not suit every use 
case. 

* No multi-operation transactions (how much ACID safety do you really need 
for an in-browser database?)
* Primary keys are represented as strings and aren't meant to be iterable. 
(Use GUIDs.)
* Every object store is a well-typed list of a certain type of objects. 
* Undefined behavior if a function-valued index or key goes to something other
than a JS object property. (The algorithm that converts lambda expressions to
property name strings uses reflection.) 


